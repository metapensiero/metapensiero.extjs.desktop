.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.extjs.desktop
.. :Created:   mar 11 dic 2012 12:17:24 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2012, 2016 Lele Gaifax
..

============================
 metapensiero.extjs.desktop
============================

This package contains a customized ExtJS 4 *desktop* application, with
some tools that simplify its deploy.

It does not (and shall not) contain any application specific logic or
resources, which should be added to another package that merely
*uses* this one as a base.
