# English (British) translations for metapensiero.extjs.desktop.
# Copyright (C) 2017 Lele Gaifax
# This file is distributed under the same license as the
# metapensiero.extjs.desktop project.
# Lele Gaifax <lele@metapensiero.it>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: metapensiero.extjs.desktop 1.38\n"
"Report-Msgid-Bugs-To: lele@metapensiero.it\n"
"POT-Creation-Date: 2022-11-13 12:42+0100\n"
"PO-Revision-Date: 2020-02-08 18:35+0100\n"
"Last-Translator: Lele Gaifax <lele@metapensiero.it>\n"
"Language: en_GB\n"
"Language-Team: en-GB <lele@lele@metapensiero.it>\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.10.3\n"

#. TRANSLATORS: this is the undefined value for a boolean column
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:438
msgid "&#160;"
msgstr "&#160;"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:239
msgid "&#160;OK&#160;"
msgstr "&#160;OK&#160;"

#. TRANSLATORS: this the "empty group" text
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:417
msgid "(None)"
msgstr "(None)"

#. TRANSLATORS: this is the format used to format a number column
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:444
msgid "0,000.00"
msgstr "0,000.00"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:456
msgid "<i>Actions</i>"
msgstr "<i>Actions</i>"

#: src/metapensiero/extjs/desktop/assets/js/upload/Dialog.js:92
msgid "Abort"
msgstr "Abort"

#: src/metapensiero/extjs/desktop/assets/js/action/AddAndDelete.js:32
msgid "Add new"
msgstr "Add new"

#. TRANSLATORS: short name of the fourth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:75
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:106
msgid "Apr"
msgstr "Apr"

#. TRANSLATORS: name of the fourth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:49
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:105
msgid "April"
msgstr "April"

#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:333
msgid "Are you sure you want to logout?"
msgstr "Are you sure you want to logout?"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:316
msgid "At least one record didn't satisfy validation constraints"
msgstr "At least one record didn't satisfy validation constraints"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:177
msgid "Attention"
msgstr "Attention"

#. TRANSLATORS: short name of the eighth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:84
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:114
msgid "Aug"
msgstr "Aug"

#. TRANSLATORS: name of the eighth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:57
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:113
msgid "August"
msgstr "August"

#: src/metapensiero/extjs/desktop/assets/js/controller/Login.js:128
msgid "Authentication..."
msgstr "Authentication..."

#: src/metapensiero/extjs/desktop/assets/js/upload/BrowseButton.js:14
msgid "Browse…"
msgstr "Browse…"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:169
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:240
msgid "Cancel"
msgstr "Cancel"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:437
msgid "Cannot decode JSON object"
msgstr "Cannot decode JSON object"

#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:54
msgid "Cannot decode JSON object obtained from "
msgstr "Cannot decode JSON object obtained from "

#: src/metapensiero/extjs/desktop/assets/js/desktop/Desktop.js:167
msgid "Cascade"
msgstr "Cascade"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:458
msgid "Changes committed successfully."
msgstr "Changes committed successfully."

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:219
msgid "Choose a month (Control+Up/Down to move years)"
msgstr "Choose a month (Control+Up/Down to move years)"

#: src/metapensiero/extjs/desktop/assets/js/grid/plugin/FilterBar.js:150
msgid "Clear all filters"
msgstr "Clear all filters"

#: src/metapensiero/extjs/desktop/assets/js/desktop/Desktop.js:181
#: src/metapensiero/extjs/desktop/assets/js/upload/Dialog.js:90
msgid "Close"
msgstr "Close"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:411
msgid "Columns"
msgstr "Columns"

#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:80
msgid "Communication error"
msgstr "Communication error"

#. TRANSLATORS: {0} is the error code, {1} the error message
#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:76
msgid "Communication error ({0} {1})"
msgstr "Communication error ({0} {1})"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:413
msgid "Communication failure!"
msgstr "Communication failure!"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:174
msgid "Confirm"
msgstr "Confirm"

#: src/metapensiero/extjs/desktop/assets/js/action/SaveAndReset.js:33
msgid "Confirm the changes."
msgstr "Confirm the changes."

#: src/metapensiero/extjs/desktop/assets/js/form/field/plugin/OperatorButton.js:59
msgid "Contains"
msgstr "Contains"

#: src/metapensiero/extjs/desktop/assets/js/upload/ItemGridPanel.js:30
msgid "Content type"
msgstr "Content type"

#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:90
#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:53
msgid "Couldn't fetch configuration"
msgstr "Couldn't fetch configuration"

#: src/metapensiero/extjs/desktop/assets/js/action/AddAndDelete.js:33
msgid "Create a new record."
msgstr "Create a new record."

#. TRANSLATORS: this is the format used to display current time in
#. the lower right corner of the desktop, see
#. http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Date for details
#. on the syntax.
#: src/metapensiero/extjs/desktop/assets/js/desktop/TrayClock.js:30
msgid "D d M, g:i A"
msgstr "D d M, g:i A"

#. TRANSLATORS: short name of the twelfth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:92
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:122
msgid "Dec"
msgstr "Dec"

#. TRANSLATORS: name of the twelfth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:65
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:121
msgid "December"
msgstr "December"

#: src/metapensiero/extjs/desktop/assets/js/action/AddAndDelete.js:41
msgid "Delete"
msgstr "Delete"

#: src/metapensiero/extjs/desktop/assets/js/action/AddAndDelete.js:42
msgid "Delete selected record."
msgstr "Delete selected record."

#: src/metapensiero/extjs/desktop/assets/js/grid/Custom.js:362
msgid "Delete {0} record?"
msgid_plural "Delete {0} records?"
msgstr[0] "Delete {0} record?"
msgstr[1] "Delete {0} records?"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:213
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:214
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:293
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:294
msgid "Disabled"
msgstr "Disabled"

#. TRANSLATORS: this is the description of current records range, where
#. {0} is the start record index, {1} the end record index and {2} the
#. total number of records
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:257
msgid "Displaying {0} - {1} of {2}"
msgstr "Displaying {0} - {1} of {2}"

#: src/metapensiero/extjs/desktop/assets/js/grid/Custom.js:363
msgid "Do you really want to delete selected record?"
msgid_plural "Do you really want to delete selected records?"
msgstr[0] "Do you really want to delete selected record?"
msgstr[1] "Do you really want to delete selected records?"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:457
msgid "Done"
msgstr "Done"

#: src/metapensiero/extjs/desktop/assets/js/toolbar/Paging.js:29
msgid "Double click to adjust the number to fit grid height."
msgstr "Double click to adjust the number to fit grid height."

#: src/metapensiero/extjs/desktop/assets/js/controller/Login.js:140
#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:418
#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:431
#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:445
#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:479
msgid "Error"
msgstr "Error"

#. TRANSLATORS: this is a date format, see
#. http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Date
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:222
msgid "F Y"
msgstr "F Y"

#. TRANSLATORS: this is a date format, see
#. http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Date
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:229
msgid "F d, Y"
msgstr "F d, Y"

#. TRANSLATORS: short name of the second month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:71
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:102
msgid "Feb"
msgstr "Feb"

#. TRANSLATORS: name of the second month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:45
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:101
msgid "February"
msgstr "February"

#: src/metapensiero/extjs/desktop/assets/js/upload/ItemGridPanel.js:28
msgid "Filename"
msgstr "Filename"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:249
msgid "First Page"
msgstr "First Page"

#. TRANSLATORS: short name of the day “Friday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:156
msgid "Fri"
msgstr "Fri"

#. TRANSLATORS: name of the day “Friday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:140
msgid "Friday"
msgstr "Friday"

#: src/metapensiero/extjs/desktop/assets/js/form/field/plugin/OperatorButton.js:49
msgid "Greater than"
msgstr "Greater than"

#: src/metapensiero/extjs/desktop/assets/js/form/field/plugin/OperatorButton.js:39
msgid "Greater than or equal"
msgstr "Greater than or equal"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:418
msgid "Group By This Field"
msgstr "Group By This Field"

#: src/metapensiero/extjs/desktop/assets/js/grid/plugin/FilterBar.js:118
msgid "Hide filter bar"
msgstr "Hide filter bar"

#: src/metapensiero/extjs/desktop/assets/js/desktop/Module.js:235
msgid "Initializing..."
msgstr "Initializing..."

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:411
msgid "Internal server error!"
msgstr "Internal server error!"

#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:91
msgid "Invalid response from "
msgstr "Invalid response from "

#: src/metapensiero/extjs/desktop/assets/js/form/field/plugin/OperatorButton.js:29
msgid "Is equal to"
msgstr "Is equal to"

#: src/metapensiero/extjs/desktop/assets/js/form/field/plugin/OperatorButton.js:34
msgid "Is not equal to"
msgstr "Is not equal to"

#. TRANSLATORS: short name of the first month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:69
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:100
msgid "Jan"
msgstr "Jan"

#. TRANSLATORS: name of the first month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:43
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:99
msgid "January"
msgstr "January"

#. TRANSLATORS: short name of the seventh month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:82
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:112
msgid "Jul"
msgstr "Jul"

#. TRANSLATORS: name of the seventh month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:55
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:111
msgid "July"
msgstr "July"

#. TRANSLATORS: short name of the sixth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:80
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:110
msgid "Jun"
msgstr "Jun"

#. TRANSLATORS: name of the sixth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:53
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:109
msgid "June"
msgstr "June"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:252
msgid "Last Page"
msgstr "Last Page"

#: src/metapensiero/extjs/desktop/assets/js/form/field/plugin/OperatorButton.js:54
msgid "Less than"
msgstr "Less than"

#: src/metapensiero/extjs/desktop/assets/js/form/field/plugin/OperatorButton.js:44
msgid "Less than or equal"
msgstr "Less than or equal"

#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:396
#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:659
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:18
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:32
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:37
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:176
msgid "Loading..."
msgstr "Loading..."

#: src/metapensiero/extjs/desktop/assets/js/window/Login.js:55
msgid "Login"
msgstr "Login"

#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:198
msgid "Logout"
msgstr "Logout"

#. TRANSLATORS: short name of the third month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:73
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:104
msgid "Mar"
msgstr "Mar"

#. TRANSLATORS: name of the third month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:47
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:103
msgid "March"
msgstr "March"

#: src/metapensiero/extjs/desktop/assets/js/desktop/Desktop.js:179
msgid "Maximize"
msgstr "Maximize"

#. TRANSLATORS: name of the fifth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:51
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:107
msgid "May"
msgstr "May"

#. TRANSLATORS: short name of the fifth month, remove the
#. “(short)” marker, added only to disambiguate
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:78
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:108
msgid "May (short)"
msgstr "May"

#: src/metapensiero/extjs/desktop/assets/js/desktop/TaskBar.js:35
msgid "Menu"
msgstr "Menu"

#: src/metapensiero/extjs/desktop/assets/js/desktop/Desktop.js:178
msgid "Minimize"
msgstr "Minimize"

#: src/metapensiero/extjs/desktop/assets/js/action/ModifyDetails.js:41
msgid "Modify"
msgstr "Modify"

#: src/metapensiero/extjs/desktop/assets/js/action/ModifyDetails.js:42
msgid "Modify details of the selected record."
msgstr "Modify details of the selected record."

#. TRANSLATORS: short name of the day “Monday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:148
msgid "Mon"
msgstr "Mon"

#. TRANSLATORS: name of the day “Monday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:132
msgid "Monday"
msgstr "Monday"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:424
msgid "Name"
msgstr "Name"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:217
msgid "Next Month (Control+Right)"
msgstr "Next Month (Control+Right)"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:251
msgid "Next Page"
msgstr "Next Page"

#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:98
#: src/metapensiero/extjs/desktop/assets/js/grid/plugin/FilterBar.js:193
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:171
msgid "No"
msgstr "No"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:258
msgid "No data to display"
msgstr "No data to display"

#: src/metapensiero/extjs/desktop/assets/js/controller/Login.js:143
msgid "No response from the server"
msgstr "No response from the server"

#. TRANSLATORS: short name of the eleventh month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:90
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:120
msgid "Nov"
msgstr "Nov"

#. TRANSLATORS: name of the eleventh month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:63
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:119
msgid "November"
msgstr "November"

#: src/metapensiero/extjs/desktop/assets/js/upload/Dialog.js:89
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:168
msgid "OK"
msgstr "OK"

#. TRANSLATORS: short name of the tenth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:88
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:118
msgid "Oct"
msgstr "Oct"

#. TRANSLATORS: name of the tenth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:61
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:117
msgid "October"
msgstr "October"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:245
msgid "Page"
msgstr "Page"

#: src/metapensiero/extjs/desktop/assets/js/toolbar/Paging.js:25
msgid "Page size"
msgstr "Page size"

#: src/metapensiero/extjs/desktop/assets/js/window/Login.js:92
msgid "Password"
msgstr "Password"

#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:341
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:263
msgid "Please Wait..."
msgstr "Please Wait..."

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:329
msgid "Please enter the URL for the link:"
msgstr "Please enter the URL for the link:"

#: src/metapensiero/extjs/desktop/assets/js/window/Login.js:21
msgid "Please login"
msgstr "Please login"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:398
msgid "Please wait"
msgstr "Please wait"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:218
msgid "Previous Month (Control+Left)"
msgstr "Previous Month (Control+Left)"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:250
msgid "Previous Page"
msgstr "Previous Page"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:175
msgid "Prompt"
msgstr "Prompt"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:253
msgid "Refresh"
msgstr "Refresh"

#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:61
msgid "Remote failure"
msgstr "Remote failure"

#: src/metapensiero/extjs/desktop/assets/js/upload/Dialog.js:94
msgid "Remove all"
msgstr "Remove all"

#: src/metapensiero/extjs/desktop/assets/js/upload/Dialog.js:93
msgid "Remove selected"
msgstr "Remove selected"

#: src/metapensiero/extjs/desktop/assets/js/action/SaveAndReset.js:47
#: src/metapensiero/extjs/desktop/assets/js/desktop/Desktop.js:177
msgid "Restore"
msgstr "Restore"

#: src/metapensiero/extjs/desktop/assets/js/action/SaveAndReset.js:48
msgid "Rollback the changes."
msgstr "Rollback the changes."

#. TRANSLATORS: short name of the day “Saturday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:158
msgid "Sat"
msgstr "Sat"

#. TRANSLATORS: name of the day “Saturday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:142
msgid "Saturday"
msgstr "Saturday"

#: src/metapensiero/extjs/desktop/assets/js/action/SaveAndReset.js:32
msgid "Save"
msgstr "Save"

#. TRANSLATORS: {0} is the number of files, {1} the total size
#: src/metapensiero/extjs/desktop/assets/js/upload/StatusBar.js:45
msgid "Selected {0} file, {1}"
msgid_plural "Selected {0} files, {1}"
msgstr[0] "Selected {0} file, {1}"
msgstr[1] "Selected {0} files, {1}"

#. TRANSLATORS: short name of the nineth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:86
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:116
msgid "Sep"
msgstr "Sep"

#. TRANSLATORS: name of the nineth month
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:59
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:115
msgid "September"
msgstr "September"

#: src/metapensiero/extjs/desktop/assets/js/grid/plugin/FilterBar.js:112
msgid "Show filter bar"
msgstr "Show filter bar"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:419
msgid "Show in Groups"
msgstr "Show in Groups"

#: src/metapensiero/extjs/desktop/assets/js/upload/ItemGridPanel.js:29
msgid "Size"
msgstr "Size"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:409
msgid "Sort Ascending"
msgstr "Sort Ascending"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:410
msgid "Sort Descending"
msgstr "Sort Descending"

#: src/metapensiero/extjs/desktop/assets/js/form/field/plugin/OperatorButton.js:64
msgid "Starts with"
msgstr "Starts with"

#: src/metapensiero/extjs/desktop/assets/js/upload/ItemGridPanel.js:31
msgid "Status"
msgstr "Status"

#. TRANSLATORS: short name of the day “Sunday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:146
msgid "Sun"
msgstr "Sun"

#. TRANSLATORS: name of the day “Sunday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:130
msgid "Sunday"
msgstr "Sunday"

#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:332
msgid "Terminate Session"
msgstr "Terminate Session"

#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:199
msgid "Terminate the application."
msgstr "Terminate the application."

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:295
msgid "The date in this field must be after {0}"
msgstr "The date in this field must be after {0}"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:296
msgid "The date in this field must be before {0}"
msgstr "The date in this field must be before {0}"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:356
msgid "The field “{0}” cannot be left empty"
msgstr "The field “{0}” cannot be left empty"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:274
msgid "The maximum length for this field is {0}"
msgstr "The maximum length for this field is {0}"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:287
msgid "The maximum value for this field is {0}"
msgstr "The maximum value for this field is {0}"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:273
msgid "The minimum length for this field is {0}"
msgstr "The minimum length for this field is {0}"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:286
msgid "The minimum value for this field is {0}"
msgstr "The minimum value for this field is {0}"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:461
msgid "The time in this field must be equal to or after {0}"
msgstr "The time in this field must be equal to or after {0}"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:462
msgid "The time in this field must be equal to or before {0}"
msgstr "The time in this field must be equal to or before {0}"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:268
msgid "The value in this field is invalid"
msgstr "The value in this field is invalid"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:212
msgid "This date is after the maximum date"
msgstr "This date is after the maximum date"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:211
msgid "This date is before the minimum date"
msgstr "This date is before the minimum date"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:275
msgid "This field is required"
msgstr "This field is required"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:317
msgid "This field should be a URL in the format"
msgstr "This field should be a URL in the format"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:312
msgid "This field should be an email address in the format"
msgstr "This field should be an email address in the format"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:322
msgid "This field should only contain letters and _"
msgstr "This field should only contain letters and _"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:323
msgid "This field should only contain letters, numbers and _"
msgstr "This field should only contain letters, numbers and _"

#. TRANSLATORS: short name of the day “Thursday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:154
msgid "Thu"
msgstr "Thu"

#. TRANSLATORS: name of the day “Thursday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:138
msgid "Thursday"
msgstr "Thursday"

#: src/metapensiero/extjs/desktop/assets/js/desktop/Desktop.js:166
msgid "Tile"
msgstr "Tile"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:210
msgid "Today"
msgstr "Today"

#. TRANSLATORS: short name of the day “Tuesday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:150
msgid "Tue"
msgstr "Tue"

#. TRANSLATORS: name of the day “Tuesday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:134
msgid "Tuesday"
msgstr "Tuesday"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:450
msgid "Unknown error"
msgstr "Unknown error"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:432
msgid "Unrecognized response"
msgstr "Unrecognized response"

#: src/metapensiero/extjs/desktop/assets/js/desktop/App.js:62
msgid "Unsuccessful response from "
msgstr "Unsuccessful response from "

#: src/metapensiero/extjs/desktop/assets/js/upload/Dialog.js:91
msgid "Upload"
msgstr "Upload"

#. TRANSLATORS: {0} is the completion percentage, {1} the number of
#. uploaded files, {2} the total number of files
#: src/metapensiero/extjs/desktop/assets/js/upload/StatusBar.js:55
msgid "Upload progress {0}% ({1} of {2} file)"
msgid_plural "Upload progress {0}% ({1} of {2} files)"
msgstr[0] "Upload progress {0}% ({1} of {2} file)"
msgstr[1] "Upload progress {0}% ({1} of {2} files)"

#: src/metapensiero/extjs/desktop/assets/js/window/Login.js:85
msgid "Username"
msgstr "Username"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:315
#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:354
msgid "Validation error"
msgstr "Validation error"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:425
msgid "Value"
msgstr "Value"

#. TRANSLATORS: short name of the day “Wednesday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:152
msgid "Wed"
msgstr "Wed"

#. TRANSLATORS: name of the day “Wednesday”
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:136
msgid "Wednesday"
msgstr "Wednesday"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:446
msgid "Write failed"
msgstr "Write failed"

#: src/metapensiero/extjs/desktop/assets/js/data/Store.js:397
msgid "Writing changes..."
msgstr "Writing changes..."

#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:98
#: src/metapensiero/extjs/desktop/assets/js/grid/plugin/FilterBar.js:192
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:170
msgid "Yes"
msgstr "Yes"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:473
msgid "You must select at least one item in this group"
msgstr "You must select at least one item in this group"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:478
msgid "You must select one item in this group"
msgstr "You must select one item in this group"

#: src/metapensiero/extjs/desktop/assets/js/form/Panel.js:67
msgid "[Mandatory]"
msgstr "[Mandatory]"

#: src/metapensiero/extjs/desktop/assets/js/grid/plugin/FilterBar.js:170
msgid "[empty]"
msgstr "[empty]"

#. TRANSLATORS: this determines the position of the currency sign,
#. "0" (zero) means it will be inserted at the beginning, with
#. any other integer value it will be appended to the formatted
#. value.
#: src/metapensiero/extjs/desktop/assets/js/form/field/CurrencyField.js:42
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:195
msgid "currencyAtEnd"
msgstr "1"

#. TRANSLATORS: this is the number of decimal places used
#. to format currency values, must be an integer value
#: src/metapensiero/extjs/desktop/assets/js/form/field/CurrencyField.js:46
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:200
msgid "currencyPrecision"
msgstr "2"

#. TRANSLATORS: this is the character used as currency sign, "$"
#: src/metapensiero/extjs/desktop/assets/js/form/field/CurrencyField.js:36
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:189
msgid "currencySign"
msgstr "£"

#. TRANSLATORS: this is the number of decimal digits
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:284
msgid "decimalPrecision"
msgstr "2"

#. TRANSLATORS: this is the character used as decimal separator, "."
#: src/metapensiero/extjs/desktop/assets/js/form/field/CurrencyField.js:34
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:187
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:281
msgid "decimalSeparator"
msgstr "."

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:430
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:436
msgid "false"
msgstr "no"

#. TRANSLATORS: this is the format used to display time of the day, see
#. http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Date for details
#. on the syntax.
#. TRANSLATORS: this is a time format, see
#. http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Date
#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:112
#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:711
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:466
msgid "g:i A"
msgstr "g:i A"

#. TRANSLATORS: this is a |-separated list of recognized time formats
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:468
msgid "g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H"
msgstr "g:ia|g:iA|g:i a|g:i A|h:i|g:i|H:i|ga|ha|gA|h a|g a|g A|gi|hi|gia|hia|g|H"

#. TRANSLATORS: this is the format used to display dates, see
#. http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Date for details
#. on the syntax.
#. TRANSLATORS: this is a date format, see
#. http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Date
#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:105
#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:719
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:204
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:451
msgid "m/d/Y"
msgstr "d/m/Y"

#. TRANSLATORS: this is the format used to display timestamps, see
#. http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Date for details
#. on the syntax.
#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:119
#: src/metapensiero/extjs/desktop/assets/js/data/MetaData.js:718
msgid "m/d/Y g:i:s A"
msgstr "m/d/Y g:i:s A"

#. TRANSLATORS: this is a |-separated list of recognized date formats
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:302
msgid "m/d/Y|m-d-y|m-d-Y|m/d|m-d|md|mdy|mdY|d|Y-m-d"
msgstr "d/m/Y|d-m-y|d-m-Y|d/m|d-m|dm|dmy|dmY|d|Y-m-d"

#. TRANSLATORS: this is a short date format, see
#. http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Date
#. TRANSLATORS: this is a date format, see
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:226
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:300
msgid "m/d/y"
msgstr "d/m/y"

#. TRANSLATORS: this is a date format, see
#. http://docs.sencha.com/extjs/4.2.1/#!/api/Ext.Date
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:428
msgid "m/j/Y"
msgstr "j/m/Y"

#. TRANSLATORS: this is the text that goes after the current page number,
#. where {0} is the total number of pages
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:248
msgid "of {0}"
msgstr "of {0}"

#: src/metapensiero/extjs/desktop/assets/js/upload/Item.js:14
msgid "ready"
msgstr "ready"

#. TRANSLATORS: this is the "index" of the first day of the week
#. 0 means Sunday, 1 Monday and so on.
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:233
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:306
msgid "startDay"
msgstr "startDay"

#. TRANSLATORS: this is the character used as thousand separator, ","
#: src/metapensiero/extjs/desktop/assets/js/form/field/CurrencyField.js:32
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:185
msgid "thousandSeparator"
msgstr ","

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:429
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:435
msgid "true"
msgstr "yes"

#: src/metapensiero/extjs/desktop/assets/js/upload/Item.js:16
msgid "uploaded"
msgstr "uploaded"

#: src/metapensiero/extjs/desktop/assets/js/upload/Item.js:17
msgid "uploaderror"
msgstr "uploaderror"

#: src/metapensiero/extjs/desktop/assets/js/upload/Item.js:15
msgid "uploading"
msgstr "uploading"

#. TRANSLATORS: this is just an example of an email address
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:315
msgid "user@example.com"
msgstr "user@example.com"

#. TRANSLATORS: this is just an example of an HTTP host name
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:320
msgid "www.example.com"
msgstr "www.example.com"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:223
msgid "{0} (Spacebar)"
msgstr "{0} (Spacebar)"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:297
msgid "{0} is not a valid date - it must be in the format {1}"
msgstr "{0} is not a valid date - it must be in the format {1}"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:288
msgid "{0} is not a valid number"
msgstr "{0} is not a valid number"

#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:463
msgid "{0} is not a valid time"
msgstr "{0} is not a valid time"

#. TRANSLATORS: this is the default text to show while
#. dragging, where {0} is the number of selected items and {1}
#. is empty when number is exactly 1, otherwise the character
#. 's' (to form the English plural form)
#: src/metapensiero/extjs/desktop/templates/extjs-l10n.mako:27
msgid "{0} selected row{1}"
msgstr "{0} selected row{1}"

